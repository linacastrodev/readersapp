import { Injectable } from "@angular/core";
import { throwError } from "rxjs";
import {
  HttpClient,
  HttpHeaders,
  HttpErrorResponse,
} from "@angular/common/http";
import { ToastController, LoadingController } from "@ionic/angular";
import { EventService } from "./event.service";

@Injectable({
  providedIn: "root",
})
export class ApiService {
  apiToken: any;
  loading: HTMLIonLoadingElement;
  apiUrl = ["https://www.json-generator.com/"];

  constructor(
    private http: HttpClient,
    private loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    private events: EventService
  ) {}

  getData(url: string, base: number = 0): Promise<any> {
      return this.http
        .get(this.apiUrl[base] + url)
        .toPromise()
        .then((res) => res)
        .catch((err) => console.log(err));
  }
  async presentLoading(myMessage = "Cargando...") {
    this.loading = await this.loadingCtrl.create({
      message: myMessage,
    });
    return await this.loading.present();
  }

  async dismissLoading() {
    return await this.loading.dismiss();
  }

}
