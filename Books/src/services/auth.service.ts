import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import {
  ToastController,
  Platform,
  LoadingController,
  NavController,
} from "@ionic/angular";
import { BehaviorSubject } from "rxjs";
import { ApiService } from "./api.service";

@Injectable({
  providedIn: "root",
})
export class AuthService {
  authState = new BehaviorSubject(false);
  loading: HTMLIonLoadingElement;

  constructor(
    private platform: Platform,
    private api: ApiService,
    private loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    private navCtrl: NavController
  ) {
    this.platform.ready().then(() => { });
  }
  getUserData() {
    return this.api.getData("/api/json/get/cgzWeKAQMO").then((res) => {
      if (res) {
        return res;
      }
    }).catch((err) => {
      console.log(err);
    });
  }
  getBookData() {
    return this.api.getData("/api/json/get/bUMICSDLyq").then((res) => {
      if (res) {
        return res;
      }
    }).catch((err) => {
      console.log(err);
    });
    
  }
  async presentLoading(message = "Cargando...") {
    this.loading = await this.loadingCtrl.create({
      message,
    });
    return await this.loading.present();
  }

  async dismissLoading() {
    return await this.loading.dismiss();
  }

  /*async isAuthenticated() {
    const apiToken = (await this.storage.get("apiToken")) || null;
    if (apiToken) {
      return true;
    } else {
      return false;
    }
  }*/
}
