import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/services/auth.service';

@Component({
  selector: 'app-singlebook',
  templateUrl: './singlebook.page.html',
  styleUrls: ['./singlebook.page.scss'],
})
export class SinglebookPage implements OnInit {
  data = [];
  constructor(private auth: AuthService) { }

  ngOnInit() {
    this.getBookData();
}

  getBookData(){
    this.auth.getBookData().then((response) => {
        this.data.push(response);
    });
    return this.data;
  } 


}
