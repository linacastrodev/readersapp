import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/services/auth.service';
@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit {
  data = [];
  constructor(private auth: AuthService) {}

  ngOnInit(){
    this.getUserData();
  }
  getUserData(){
    this.auth.getUserData()
    .then((response)=>{
      this.data = response;
    });
      return this.data;
  }
}
