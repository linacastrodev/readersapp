import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/services/auth.service';
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit{
  data =[];
  constructor(private auth: AuthService) {}

  ngOnInit(){
    this.getBookData();    
  }

  getBookData() {
    this.auth.getBookData().then((res) => {
      console.log('Res: ', res);
      this.data.push(res);
    });
    return this.data
  }
}
