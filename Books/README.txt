Hi, Before opening the project and compiling it on the different platforms, the first thing you should do is install the dependencies of the starter with the command: npm install.
Remember that you must be inside the project folder.

This starter is designed only for cell phone & iPad resolutions.

If you have any questions regarding the design or want support from it, contact the following email: linacastrodev@gmail.com

or in my social networks, I appear as: @lirrums.


THIS STARTER IS MADE WITH LOVE